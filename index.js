const https = require('https')
const fs = require('fs')
const express = require('express')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const session = require('express-session')
const crypto = require('crypto')
const app = express()
const csrf = require('csurf')
const cookieParser = require('cookie-parser')
const { home } = require('./home');
const multer  = require('multer')
const path = require('path')
const auth = require('basic-auth')
const maxSize = 4 * 1000 * 1000;
// const upload = multer({ dest: 'uploaded_images/' })
const options = {
  key: fs.readFileSync(process.env.SSL_KEY),
  cert: fs.readFileSync(process.env.SSL_CERT)
}
const SESSION_SECRET =
  process.env.SESSION_SECRET || crypto.randomBytes(64).toString('hex');
let images = ``

const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now())
  }
})
const upload = multer({
 limits:{
  fileSize: maxSize
 },
 fileFilter: function (req, file, cb) {
   const filetypes = /jpeg|jpg|png/
   const mimetype = filetypes.test(file.mimetype)
   const extname = filetypes.test(path.extname(file.originalname).toLowerCase())
   if (mimetype && extname && file.originalname === escape(file.originalname)) {
     return cb(null, true)
   }
   cb("Error: File upload not valid")
 },
 dest: 'uploaded_images/'
})

app.use(upload.single('img'))
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"],
      imgSrc: ['*'],
      upgradeInsecureRequests: true,
    },
  })
)
app.use(helmet.frameguard({ action: 'deny' }))
app.use(helmet.noSniff())
app.use(
  helmet.hsts({ maxAge: 31536000, includeSubDomains: true, preload: true })
)
app.use(helmet.ieNoOpen())
app.use(helmet.referrerPolicy({ policy: 'no-referrer' }))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(csrf({ cookie: true }))
app.get('/', function(req, res) {
  res.send(
    home({
      csrfToken: req.csrfToken()
    })
  )
})

app.post('/upload', function(req, res, next) {
  fs.chmodSync(`./uploaded_images/${req.file.filename}`, '666')
  res.redirect(`/display?image=${req.file.filename}`)
})

app.get('/display', function(req, res, next) {
  if (!auth(req)) {
    res.set('WWW-Authenticate', 'Basic realm="image access"')
    return res.status(401).send()
  }
  let { name, pass } = auth(req)
  console.log('name', name)
  name = escape(name)
  console.log('name22222', name)
  pass = escape(pass)
  if (name === process.env.USER && pass === process.env.PASSWORD) {
    fs.readFile(`./uploaded_images/${req.query.image}`, function(err, data){
      if(err) throw err
      res.header('Content-Type', 'image/jpg')
      res.send(data)
   })
  } else {
    return res.status(401).send('bad creds')
  }
})

https.createServer(options, app).listen(process.env.PORT)
