# File Upload

Welcome to File Upload app. This app is using NodeJS.

## Getting Started

You need to install the dependencies with `npm install`

Before starting the project, you need to fill the <b>.env.sample</b> file and when it's done, just type `source .env.sample` in the terminal.
This is to feed the environment's vars.

You can just now start the project typing `npm run dev` or `node index.js` in the terminal.

Well Done ! Your project is now running on the port you just set
in the .env.sample file !

## Important informations

The project will run on [https://fileupload.dev:8080](https://fileupload.dev:8080).
As said before, you can change the port in the <b>.env.sample</b> file. The server
is running on https protocol. If you want to change it, you need to install [mkcert](https://github.com/FiloSottile/mkcert)
and to generate the <b>.pem<b> files.

Do not forget to update the <b>.env.sample</b> variables, they are looking for theses files.
After every change, you need to `source .env.sample` to update the environment's vars values.

If you can't access the site, you need to change your <b>/etc/hosts</b> file and refer 127.0.0.1 to fileupload.dev or whatever you want.
